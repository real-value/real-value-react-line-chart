import React, {useState} from 'react';
import { render} from 'react-dom';

import { LineChart } from '../../src';

const App = () => {

    let initialData=[
        { name: 'Jan', value: 30 },
        { name: 'Feb', value: 10 },
        { name: 'Mar', value: 50 },
        { name: 'Apr', value: 20 },
        { name: 'May', value: 80 },
        { name: 'Jun', value: 30 },
        { name: 'July', value: 0 },
        { name: 'Aug', value: 20 },
        { name: 'Sep', value: 100 },
        { name: 'Oct', value: 55 },
        { name: 'Nov', value: 60 },
        { name: 'Dec', value: 80 },
      ]
    
    const [ data,setData] = useState(initialData)

    let randomData = (e) => {
        e.preventDefault();
        const newData = data.map(d => ({
            name: d.name,
            value: Math.floor((Math.random() * 100) + 1)
          }))
        setData(newData)
    }


    return <div>
         <button onClick={randomData}>Randomize data</button> 
        <h1>real-value-react-line-chart Demo</h1>
        <LineChart data={data}/>
    </div>
}
render(<App />, document.querySelector('#demo'));

