import { describe } from 'riteway';
import render from 'riteway/render-component';
import React from 'react';

import LineChartComponent from '../src';

describe.skip('LineChartComponent should render properly', async (assert) => {

  let data=[
    { name: 'Jan', value: 30 },
    { name: 'Feb', value: 10 },
    { name: 'Mar', value: 50 }
  ]

  const createLineChart = () =>
    render(<LineChartComponent data={data} />)

    // const count = 3;
    const $ = createLineChart();

    assert({
      given: 'a line component',
      should: 'Should render ...',
      actual: $('span')
        .html()
        .trim(),
      expected: ''
    });
});
