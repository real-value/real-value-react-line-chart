import React, { useEffect, useRef } from 'react';
import { select, selectAll } from 'd3-selection';
import { transition } from 'd3-transition';

const Line =  (props) =>{
  const { xScale, yScale, data, lineGenerator } = props;
  let linegroup = useRef(null);

  useEffect(()=>{
    const node = linegroup.current;
  
    const initialData = data.map(d => ({
      name: d.name,
      value: 0
    }));

    let lineg = select(node)

    lineg
      .append('path')
      .datum(initialData)
      .attr('id', 'line')
      .attr('stroke', 'black')
      .attr('stroke-width', 2)
      .attr('fill', 'none')
      .attr('d', lineGenerator);

      updateChart()
    })
        
  function updateChart() {

    const t = transition().duration(1000);

    const line = select('#line');
    const dot = selectAll('.circle');

    line
      .datum(data)
      .transition(t)
      .attr('d', lineGenerator);

    // dot
    //   .data(data)
    //   .transition(t)
    //   .attr('cx', (d, key) => xScale(key))
    //   .attr('cy', d => yScale(d.count));
  }
  
  //ref={linegroup}
  return <g className="line-group" ref={linegroup} />;
}

export default Line

