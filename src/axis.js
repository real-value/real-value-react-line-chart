import React, { useEffect, useRef } from 'react';
import { select, selectAll } from 'd3-selection';
import { axisBottom, axisLeft } from 'd3-axis';
import { transition } from 'd3-transition';

const Axis =  (props) =>{
    const { scale , orient, transform, ticks, name } = props;
    const axis = useRef(null)

    function renderAxis() {
        const node = axis.current;
        let axisContent
        if (orient === 'bottom') {
            axisContent = axisBottom(scale);
        }
        if (orient === 'left') {
            axisContent = axisLeft(scale).ticks(ticks);
        }
        select(node).call(axisContent);
    }
    function updateAxis() {
        const t = transition().duration(1000)

        if (orient === 'left') {
        const axisContent = axisLeft(scale).ticks(ticks); 
        selectAll(`.${orient}`).transition(t).call(axisContent)
        }
    }

    useEffect(()=>{
        renderAxis();
        updateAxis();
    })

    return <g
        ref={axis}
        transform={transform}
        className={`${orient} axis`}
      />
    
}
export default Axis